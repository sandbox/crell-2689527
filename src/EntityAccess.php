<?php

namespace Drupal\workspace_permissions;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\multiversion\Entity\WorkspaceInterface;

/**
 * Service wrapper for hooks relating to entity access control.
 */
class EntityAccess {

  /**
   * Hook bridge;
   *
   * @see hook_entity_access()
   * @see hook_ENTITY_TYPE_access()
   *
   * @param WorkspaceInterface $workspace
   * @param string $operation
   * @param AccountInterface $account
   *
   * @return AccessResult
   */
  public function workspaceAccess(WorkspaceInterface $workspace, $operation, AccountInterface $account) {

    $operations = [
      'view' => ['any' => 'view_any_workspace', 'own' => 'view_own_workspace'],
      'update' => ['any' => 'edit_any_workspace', 'own' => 'edit_own_workspace'],
      'delete' => ['any' => 'delete_any_workspace', 'own' => 'delete_own_workspace'],
    ];

    $result = AccessResult::allowedIfHasPermission($account, $operations[$operation]['any'])
    ->orIf(
      AccessResult::allowedIf($workspace->getOwnerId() == $account->id())
        ->andIf(AccessResult::allowedIfHasPermission($account, $operations[$operation]['own']))
    );

    return $result;

    //return AccessResult::neutral();
  }

  /**
   * Hook bridge;
   *
   * @see hook_create_access();
   * @see hook_ENTITY_TYPE_create_access().
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   * @param array $context
   * @param $entity_bundle
   *
   * @return AccessResult
   */
  public function workspaceCreateAccess(AccountInterface $account, array $context, $entity_bundle) {
    return AccessResult::allowedIfHasPermission($account, 'create_workspace');
  }

  /**
   * Returns an array of workspace-specific permissions.
   *
   * Note: This approach assumes that a site will have only a small number
   * of workspace entities, under a dozen. If there are many dozens of
   * workspaces defined then this approach will have scaling issues.
   *
   * @return array
   *   The workspace permissions.
   */
  public function workspacePermissions() {
    $perms = [];
    /*
    /* @var \Drupal\workbench_moderation\ModerationStateInterface[] $states
    $states = ModerationState::loadMultiple();
    /* @var \Drupal\workbench_moderation\ModerationStateTransitionInterface $transition
    foreach (ModerationStateTransition::loadMultiple() as $id => $transition) {
      $perms['use ' . $id . ' transition'] = [
        'title' => $this->t('Use the %transition_name transition', [
          '%transition_name' => $transition->label(),
        ]),
        'description' => $this->t('Move content from %from state to %to state.', [
          '%from' => $states[$transition->getFromState()]->label(),
          '%to' => $states[$transition->getToState()]->label(),
        ]),
      ];
    }

    return $perms;
    */
  }

}
