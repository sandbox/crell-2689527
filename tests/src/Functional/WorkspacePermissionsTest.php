<?php

namespace Drupal\Tests\workspace_permissions\Functional;

use Drupal\simpletest\BrowserTestBase;


/**
 *
 * @group workspace_permissions
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 *
 */
class WorkspacePermissionsTest extends BrowserTestBase {

  public static $modules = ['workspace', 'workspace_permissions'];

  protected $editor;

  public function setUp() {
    parent::setUp();

    $this->editor = $this->drupalCreateUser([
      'access administration pages',
      'create_workspace',
      'administer workspaces', // @todo Make this unnecessary for creating.
    ]);
  }

  public function testCreateWorkspace() {

    // Login as a limited-access user and create a workspace.
    $this->drupalLogin($this->editor);
    $session = $this->getSession();

    $session->visit('/admin/structure/workspace/add');

    $page = $session->getPage();
    $page->fillField('label', 'Bears');
    $page->fillField('machine_name', 'bears');
    $page->findButton(t('Save'))->click();

    $session->getPage()->hasContent('Bears (bears)');

    // Now edit that same workspace; We shouldn't be able to do so, since
    // we don't have edit permissions.
  }
}
